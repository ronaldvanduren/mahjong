module.exports = function(grunt) {
    grunt.initConfig({
        browserify: {
            js: {
                src: 'app/js/app.js',
                dest: 'dist/js/app.js',
                options: {
                    external: ['angular'],
                    debug: true,
                    browserifyOptions: { debug: true }
                }
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    trace : true
                },
                files: {
                    'dist/css/bamboo_theme.css' : 'app/css/bamboo_theme.scss',
                    'dist/css/wood_theme.css' : 'app/css/wood_theme.scss',
                    'dist/css/tiles.css' : 'app/css/tiles.scss'
                    //'dist/css/game.css' : 'app/css/game.scss',
                    //'dist/css/app.css' : 'app/css/app.scss',
                    //'dist/css/testTest.css' : 'app/css/testTest.scss'
                }
            }
        },

        copy: {
            all: {
                // This copies all the html and css into the dist/ folder
                expand: true,
                cwd: 'app/',
                src: ['**/*.html', '**/*.css', '**/*.jpg'],
                dest: 'dist/'
            }
        },
        watch: {
            js: {
                files: "app/**/*.js",
                tasks: "browserify"
            },
            html: {
                files: 'app/**/*.html',
                tasks: 'copy'
            },
            css: {
                files: 'app/**/*.scss',
                tasks: 'sass'
            },
            images: {
                files: 'app/**/*.jpg',
                tasks: 'copy'
            }
        }
    });

    // Load the npm installed tasks
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // The default tasks to run when you type: grunt
    grunt.registerTask('default', ['browserify', 'copy', 'sass']);
};
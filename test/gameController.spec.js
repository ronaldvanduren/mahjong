
describe('game controller functionality', function(){
    it('should return true if there are still matches possible', function(){

        gameService.setCurrentGame(tiles, {id: "", players:[]}, true);

        gameController = $controller("GameCtrl", {$scope: scope, $state: $state, Authentication: authenticationService,
            Games: gamesService, Game: gameService, GameBoard: gameBoardFactory, Sockets: socketService});

        expect(gameController.checkIfMatchesArePossible(tiles)).to.equal(true);
    });

    it('should return false if there are no matches possible', function(){
        gameService.setCurrentGame(finishedTiles, {id: "", players:[]}, true);

        gameController = $controller("GameCtrl", {$scope: scope, $state: $state, Authentication: authenticationService,
            Games: gamesService, Game: gameService, GameBoard: gameBoardFactory, Sockets: socketService});

        expect(gameController.checkIfMatchesArePossible(finishedTiles)).to.equal(false);
    });

    it('should return true if a hint can be given', function(){
        gameService.setCurrentGame(tiles, {id: "", players:[]}, true);

        scope.players = [{name: "ronald", matchedTiles: [{}, {}]}];

        gameController = $controller("GameCtrl", {$scope: scope, $state: $state, Authentication: authenticationService,
            Games: gamesService, Game: gameService, GameBoard: gameBoardFactory, Sockets: socketService});

        expect(gameController.showHint(tiles)).to.equal(true);
    });

    it('should return false if no hint can be given', function(){
        gameService.setCurrentGame(finishedTiles, {id: "", players:[]}, true);

        scope.players = [{name: "ronald", matchedTiles: [{}, {}]}];

        gameController = $controller("GameCtrl", {$scope: scope, $state: $state, Authentication: authenticationService,
            Games: gamesService, Game: gameService, GameBoard: gameBoardFactory, Sockets: socketService});


        expect(gameController.showHint(finishedTiles)).to.equal(false);
    });

    it('should return true if a match can be made', function(){
        gameService.setCurrentGame(tiles, {id: "", players:[]}, true);

        gameController = $controller("GameCtrl", {$scope: scope, $state: $state, Authentication: authenticationService,
            Games: gamesService, Game: gameService, GameBoard: gameBoardFactory, Sockets: socketService});

        var tileA = {"xPos":15,"yPos":9,"zPos":1,"tile":{"_id":80,"suit":"Character","name":"3","matchesWholeSuit":false,"__v":0,"id":"80"},"_id":"55ae8950056e5b110042f026"};
        var tileB = {"xPos":17,"yPos":15,"zPos":0,"tile":{"_id":81,"suit":"Character","name":"3","matchesWholeSuit":false,"__v":0,"id":"81"},"_id":"55ae8950056e5b110042f00c"};

        gameController.checkMatch(tileA);

        expect(gameController.checkMatch(tileB)).to.equal(true);
    });



});
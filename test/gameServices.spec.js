'use strict';
var $controller;
var authCtrl;
var scope;
var gameService;
var gamesService;
var gameBoardFactory;
var authenticationService;
var socketService;
var templateService;
var userService;
var gameController;
var configService;
var $http;
var $httpBackend;
var $rootScope;
var $location;
var $state;
var player = {_id: "r.vanduren@test.com", name: "Ronald van Duren", token: "token"};
var tiles = [{"xPos":15,"yPos":9,"zPos":1,"tile":{"_id":80,"suit":"Character","name":"3","matchesWholeSuit":false,"__v":0,"id":"80"},"_id":"55ae8950056e5b110042f026"},{"xPos":17,"yPos":7,"zPos":1,"tile":{"_id":54,"suit":"Bamboo","name":"5","matchesWholeSuit":false,"__v":0,"id":"54"},"_id":"55ae8950056e5b110042f021"},{"xPos":15,"yPos":7,"zPos":1,"tile":{"_id":22,"suit":"Circle","name":"6","matchesWholeSuit":false,"__v":0,"id":"22"},"_id":"55ae8950056e5b110042f020"},{"xPos":15,"yPos":5,"zPos":1,"tile":{"_id":69,"suit":"Bamboo","name":"9","matchesWholeSuit":false,"__v":0,"id":"69"},"_id":"55ae8950056e5b110042f01a"},{"xPos":15,"yPos":3,"zPos":1,"tile":{"_id":14,"suit":"Circle","name":"4","matchesWholeSuit":false,"__v":0,"id":"14"},"_id":"55ae8950056e5b110042f014"},{"xPos":13,"yPos":3,"zPos":1,"tile":{"_id":105,"suit":"Character","name":"9","matchesWholeSuit":false,"__v":0,"id":"105"},"_id":"55ae8950056e5b110042f013"},{"xPos":23,"yPos":15,"zPos":0,"tile":{"_id":98,"suit":"Character","name":"7","matchesWholeSuit":false,"__v":0,"id":"98"},"_id":"55ae8950056e5b110042f00f"},{"xPos":21,"yPos":15,"zPos":0,"tile":{"_id":136,"suit":"Season","name":"Summer","matchesWholeSuit":true,"__v":0,"id":"136"},"_id":"55ae8950056e5b110042f00e"},{"xPos":19,"yPos":15,"zPos":0,"tile":{"_id":6,"suit":"Circle","name":"2","matchesWholeSuit":false,"__v":0,"id":"6"},"_id":"55ae8950056e5b110042f00d"},{"xPos":17,"yPos":15,"zPos":0,"tile":{"_id":81,"suit":"Character","name":"3","matchesWholeSuit":false,"__v":0,"id":"81"},"_id":"55ae8950056e5b110042f00c"},{"xPos":15,"yPos":15,"zPos":0,"tile":{"_id":28,"suit":"Circle","name":"8","matchesWholeSuit":false,"__v":0,"id":"28"},"_id":"55ae8950056e5b110042f00b"},{"xPos":13,"yPos":15,"zPos":0,"tile":{"_id":9,"suit":"Circle","name":"3","matchesWholeSuit":false,"__v":0,"id":"9"},"_id":"55ae8950056e5b110042f00a"},{"xPos":11,"yPos":15,"zPos":0,"tile":{"_id":8,"suit":"Circle","name":"3","matchesWholeSuit":false,"__v":0,"id":"8"},"_id":"55ae8950056e5b110042f009"},{"xPos":9,"yPos":15,"zPos":0,"tile":{"_id":143,"suit":"Flower","name":"Bamboo","matchesWholeSuit":true,"__v":0,"id":"143"},"_id":"55ae8950056e5b110042f008"},{"xPos":21,"yPos":13,"zPos":0,"tile":{"_id":79,"suit":"Character","name":"2","matchesWholeSuit":false,"__v":0,"id":"79"},"_id":"55ae8950056e5b110042f004"},{"xPos":19,"yPos":13,"zPos":0,"tile":{"_id":76,"suit":"Character","name":"2","matchesWholeSuit":false,"__v":0,"id":"76"},"_id":"55ae8950056e5b110042f003"},{"xPos":17,"yPos":13,"zPos":0,"tile":{"_id":66,"suit":"Bamboo","name":"8","matchesWholeSuit":false,"__v":0,"id":"66"},"_id":"55ae8950056e5b110042f002"},{"xPos":15,"yPos":13,"zPos":0,"tile":{"_id":50,"suit":"Bamboo","name":"4","matchesWholeSuit":false,"__v":0,"id":"50"},"_id":"55ae8950056e5b110042f001"},{"xPos":13,"yPos":13,"zPos":0,"tile":{"_id":68,"suit":"Bamboo","name":"9","matchesWholeSuit":false,"__v":0,"id":"68"},"_id":"55ae8950056e5b110042f000"},{"xPos":11,"yPos":13,"zPos":0,"tile":{"_id":32,"suit":"Circle","name":"9","matchesWholeSuit":false,"__v":0,"id":"32"},"_id":"55ae8950056e5b110042efff"},{"xPos":13,"yPos":11,"zPos":0,"tile":{"_id":46,"suit":"Bamboo","name":"3","matchesWholeSuit":false,"__v":0,"id":"46"},"_id":"55ae8950056e5b110042eff7"},{"xPos":11,"yPos":11,"zPos":0,"tile":{"_id":65,"suit":"Bamboo","name":"8","matchesWholeSuit":false,"__v":0,"id":"65"},"_id":"55ae8950056e5b110042eff6"},{"xPos":19,"yPos":9,"zPos":0,"tile":{"_id":96,"suit":"Character","name":"7","matchesWholeSuit":false,"__v":0,"id":"96"},"_id":"55ae8950056e5b110042efef"},{"xPos":17,"yPos":9,"zPos":0,"tile":{"_id":140,"suit":"Flower","name":"Plum","matchesWholeSuit":true,"__v":0,"id":"140"},"_id":"55ae8950056e5b110042efee"},{"xPos":15,"yPos":9,"zPos":0,"tile":{"_id":45,"suit":"Bamboo","name":"3","matchesWholeSuit":false,"__v":0,"id":"45"},"_id":"55ae8950056e5b110042efed"},{"xPos":23,"yPos":7,"zPos":0,"tile":{"_id":112,"suit":"Wind","name":"East","matchesWholeSuit":false,"__v":0,"id":"112"},"_id":"55ae8950056e5b110042efe2"},{"xPos":21,"yPos":7,"zPos":0,"tile":{"_id":55,"suit":"Bamboo","name":"5","matchesWholeSuit":false,"__v":0,"id":"55"},"_id":"55ae8950056e5b110042efe1"},{"xPos":19,"yPos":7,"zPos":0,"tile":{"_id":23,"suit":"Circle","name":"6","matchesWholeSuit":false,"__v":0,"id":"23"},"_id":"55ae8950056e5b110042efe0"},{"xPos":17,"yPos":7,"zPos":0,"tile":{"_id":37,"suit":"Bamboo","name":"1","matchesWholeSuit":false,"__v":0,"id":"37"},"_id":"55ae8950056e5b110042efdf"},{"xPos":15,"yPos":7,"zPos":0,"tile":{"_id":126,"suit":"Dragon","name":"Red","matchesWholeSuit":false,"__v":0,"id":"126"},"_id":"55ae8950056e5b110042efde"},{"xPos":13,"yPos":7,"zPos":0,"tile":{"_id":137,"suit":"Season","name":"Winter","matchesWholeSuit":true,"__v":0,"id":"137"},"_id":"55ae8950056e5b110042efdd"},{"xPos":11,"yPos":7,"zPos":0,"tile":{"_id":61,"suit":"Bamboo","name":"7","matchesWholeSuit":false,"__v":0,"id":"61"},"_id":"55ae8950056e5b110042efdc"},{"xPos":9,"yPos":7,"zPos":0,"tile":{"_id":108,"suit":"Wind","name":"North","matchesWholeSuit":false,"__v":0,"id":"108"},"_id":"55ae8950056e5b110042efdb"},{"xPos":7,"yPos":7,"zPos":0,"tile":{"_id":59,"suit":"Bamboo","name":"6","matchesWholeSuit":false,"__v":0,"id":"59"},"_id":"55ae8950056e5b110042efda"},{"xPos":19,"yPos":5,"zPos":0,"tile":{"_id":41,"suit":"Bamboo","name":"2","matchesWholeSuit":false,"__v":0,"id":"41"},"_id":"55ae8950056e5b110042efd5"},{"xPos":17,"yPos":5,"zPos":0,"tile":{"_id":5,"suit":"Circle","name":"2","matchesWholeSuit":false,"__v":0,"id":"5"},"_id":"55ae8950056e5b110042efd4"},{"xPos":15,"yPos":5,"zPos":0,"tile":{"_id":34,"suit":"Circle","name":"9","matchesWholeSuit":false,"__v":0,"id":"34"},"_id":"55ae8950056e5b110042efd3"},{"xPos":13,"yPos":5,"zPos":0,"tile":{"_id":15,"suit":"Circle","name":"4","matchesWholeSuit":false,"__v":0,"id":"15"},"_id":"55ae8950056e5b110042efd2"},{"xPos":11,"yPos":5,"zPos":0,"tile":{"_id":56,"suit":"Bamboo","name":"6","matchesWholeSuit":false,"__v":0,"id":"56"},"_id":"55ae8950056e5b110042efd1"},{"xPos":9,"yPos":5,"zPos":0,"tile":{"_id":48,"suit":"Bamboo","name":"4","matchesWholeSuit":false,"__v":0,"id":"48"},"_id":"55ae8950056e5b110042efd0"},{"xPos":15,"yPos":3,"zPos":0,"tile":{"_id":60,"suit":"Bamboo","name":"7","matchesWholeSuit":false,"__v":0,"id":"60"},"_id":"55ae8950056e5b110042efca"},{"xPos":13,"yPos":3,"zPos":0,"tile":{"_id":90,"suit":"Character","name":"5","matchesWholeSuit":false,"__v":0,"id":"90"},"_id":"55ae8950056e5b110042efc9"},{"xPos":11,"yPos":3,"zPos":0,"tile":{"_id":111,"suit":"Wind","name":"North","matchesWholeSuit":false,"__v":0,"id":"111"},"_id":"55ae8950056e5b110042efc8"},{"xPos":9,"yPos":3,"zPos":0,"tile":{"_id":115,"suit":"Wind","name":"East","matchesWholeSuit":false,"__v":0,"id":"115"},"_id":"55ae8950056e5b110042efc7"},{"xPos":23,"yPos":1,"zPos":0,"tile":{"_id":91,"suit":"Character","name":"5","matchesWholeSuit":false,"__v":0,"id":"91"},"_id":"55ae8950056e5b110042efc4"},{"xPos":21,"yPos":1,"zPos":0,"tile":{"_id":36,"suit":"Bamboo","name":"1","matchesWholeSuit":false,"__v":0,"id":"36"},"_id":"55ae8950056e5b110042efc3"},{"xPos":19,"yPos":1,"zPos":0,"tile":{"_id":124,"suit":"Dragon","name":"Red","matchesWholeSuit":false,"__v":0,"id":"124"},"_id":"55ae8950056e5b110042efc2"},{"xPos":17,"yPos":1,"zPos":0,"tile":{"_id":43,"suit":"Bamboo","name":"2","matchesWholeSuit":false,"__v":0,"id":"43"},"_id":"55ae8950056e5b110042efc1"},{"xPos":15,"yPos":1,"zPos":0,"tile":{"_id":31,"suit":"Circle","name":"8","matchesWholeSuit":false,"__v":0,"id":"31"},"_id":"55ae8950056e5b110042efc0"},{"xPos":13,"yPos":1,"zPos":0,"tile":{"_id":107,"suit":"Character","name":"9","matchesWholeSuit":false,"__v":0,"id":"107"},"_id":"55ae8950056e5b110042efbf"}];
var finishedTiles = [{"xPos":15,"yPos":13,"zPos":1,"tile":{"_id":16,"suit":"Circle","name":"5","matchesWholeSuit":false,"__v":0,"id":"16"},"_id":"55d21f599d8afe1100e6a48c"},{"xPos":13,"yPos":13,"zPos":1,"tile":{"_id":4,"suit":"Circle","name":"2","matchesWholeSuit":false,"__v":0,"id":"4"},"_id":"55d21f599d8afe1100e6a48b"},{"xPos":11,"yPos":13,"zPos":1,"tile":{"_id":125,"suit":"Dragon","name":"Red","matchesWholeSuit":false,"__v":0,"id":"125"},"_id":"55d21f599d8afe1100e6a48a"},{"xPos":15,"yPos":11,"zPos":1,"tile":{"_id":29,"suit":"Circle","name":"8","matchesWholeSuit":false,"__v":0,"id":"29"},"_id":"55d21f599d8afe1100e6a484"},{"xPos":13,"yPos":11,"zPos":1,"tile":{"_id":64,"suit":"Bamboo","name":"8","matchesWholeSuit":false,"__v":0,"id":"64"},"_id":"55d21f599d8afe1100e6a483"},{"xPos":11,"yPos":11,"zPos":1,"tile":{"_id":49,"suit":"Bamboo","name":"4","matchesWholeSuit":false,"__v":0,"id":"49"},"_id":"55d21f599d8afe1100e6a482"},{"xPos":17,"yPos":7,"zPos":1,"tile":{"_id":27,"suit":"Circle","name":"7","matchesWholeSuit":false,"__v":0,"id":"27"},"_id":"55d21f599d8afe1100e6a473"},{"xPos":23,"yPos":3,"zPos":1,"tile":{"_id":67,"suit":"Bamboo","name":"8","matchesWholeSuit":false,"__v":0,"id":"67"},"_id":"55d21f599d8afe1100e6a464"},{"xPos":8,"yPos":1,"zPos":1,"tile":{"_id":6,"suit":"Circle","name":"2","matchesWholeSuit":false,"__v":0,"id":"6"},"_id":"55d21f599d8afe1100e6a458"},{"xPos":15,"yPos":15,"zPos":0,"tile":{"_id":9,"suit":"Circle","name":"3","matchesWholeSuit":false,"__v":0,"id":"9"},"_id":"55d21f599d8afe1100e6a456"},{"xPos":13,"yPos":15,"zPos":0,"tile":{"_id":128,"suit":"Dragon","name":"Green","matchesWholeSuit":false,"__v":0,"id":"128"},"_id":"55d21f599d8afe1100e6a455"},{"xPos":15,"yPos":13,"zPos":0,"tile":{"_id":51,"suit":"Bamboo","name":"4","matchesWholeSuit":false,"__v":0,"id":"51"},"_id":"55d21f599d8afe1100e6a451"},{"xPos":13,"yPos":13,"zPos":0,"tile":{"_id":131,"suit":"Dragon","name":"Green","matchesWholeSuit":false,"__v":0,"id":"131"},"_id":"55d21f599d8afe1100e6a450"},{"xPos":11,"yPos":13,"zPos":0,"tile":{"_id":28,"suit":"Circle","name":"8","matchesWholeSuit":false,"__v":0,"id":"28"},"_id":"55d21f599d8afe1100e6a44f"},{"xPos":15,"yPos":11,"zPos":0,"tile":{"_id":126,"suit":"Dragon","name":"Red","matchesWholeSuit":false,"__v":0,"id":"126"},"_id":"55d21f599d8afe1100e6a449"},{"xPos":13,"yPos":11,"zPos":0,"tile":{"_id":10,"suit":"Circle","name":"3","matchesWholeSuit":false,"__v":0,"id":"10"},"_id":"55d21f599d8afe1100e6a448"},{"xPos":11,"yPos":11,"zPos":0,"tile":{"_id":54,"suit":"Bamboo","name":"5","matchesWholeSuit":false,"__v":0,"id":"54"},"_id":"55d21f599d8afe1100e6a447"},{"xPos":9,"yPos":9,"zPos":0,"tile":{"_id":21,"suit":"Circle","name":"6","matchesWholeSuit":false,"__v":0,"id":"21"},"_id":"55d21f599d8afe1100e6a43d"},{"xPos":17,"yPos":7,"zPos":0,"tile":{"_id":20,"suit":"Circle","name":"6","matchesWholeSuit":false,"__v":0,"id":"20"},"_id":"55d21f599d8afe1100e6a437"},{"xPos":23,"yPos":3,"zPos":0,"tile":{"_id":24,"suit":"Circle","name":"7","matchesWholeSuit":false,"__v":0,"id":"24"},"_id":"55d21f599d8afe1100e6a428"},{"xPos":11,"yPos":3,"zPos":0,"tile":{"_id":55,"suit":"Bamboo","name":"5","matchesWholeSuit":false,"__v":0,"id":"55"},"_id":"55d21f599d8afe1100e6a423"},{"xPos":9,"yPos":3,"zPos":0,"tile":{"_id":101,"suit":"Character","name":"8","matchesWholeSuit":false,"__v":0,"id":"101"},"_id":"55d21f599d8afe1100e6a422"},{"xPos":7,"yPos":3,"zPos":0,"tile":{"_id":17,"suit":"Circle","name":"5","matchesWholeSuit":false,"__v":0,"id":"17"},"_id":"55d21f599d8afe1100e6a421"},{"xPos":5,"yPos":3,"zPos":0,"tile":{"_id":103,"suit":"Character","name":"8","matchesWholeSuit":false,"__v":0,"id":"103"},"_id":"55d21f599d8afe1100e6a420"},{"xPos":22,"yPos":1,"zPos":0,"tile":{"_id":62,"suit":"Bamboo","name":"7","matchesWholeSuit":false,"__v":0,"id":"62"},"_id":"55d21f599d8afe1100e6a41f"},{"xPos":8,"yPos":1,"zPos":0,"tile":{"_id":61,"suit":"Bamboo","name":"7","matchesWholeSuit":false,"__v":0,"id":"61"},"_id":"55d21f599d8afe1100e6a41c"}];
beforeEach(module('Mahjong'));

beforeEach(inject(function(_$rootScope_, _$controller_, $injector, _$http_, _$httpBackend_, _$state_, _$location_){
     scope = _$rootScope_.$new();

    $rootScope = _$rootScope_;
    $http = _$http_;
    $httpBackend = _$httpBackend_;
    configService = $injector.get('Config');
    gameService = $injector.get('Game');
    authenticationService = $injector.get('Authentication');
    gamesService = $injector.get('Games');
    templateService = $injector.get('Templates');
    gameBoardFactory = $injector.get('GameBoard');
    socketService = $injector.get('Sockets');
    userService = $injector.get('Users');
    $controller = _$controller_;
    $state = _$state_;

    //$stateProvider.state('home', { url: '#/home' });
    authCtrl = $controller('AuthCtrl', {$scope: scope, $location:_$location_, $state: _$state_});
    authenticationService.setCurrentUser(player._id, player.token);

}));

describe('GameService functionality', function(){
    it('should return a playable game after it has been set', function(){
        var Game = {id: "12345", players: []}
        gameService.setCurrentGame([], Game, true);

        var currentGame = gameService.getCurrentGame();

        currentGame.id.should.be.a('string');
        currentGame.id.should.equal("12345");
    });

    it('should clear the current game', function(){
        var Game = {id: "12345", players: []}
        gameService.setCurrentGame([], Game, true);

        gameService.clearCurrentGame();

        var currentGame = gameService.getCurrentGame();


        currentGame.id.should.be.a('string');
        currentGame.id.should.equal("");
    });

    it('should get the history of a specific game and return an array of tiles', function(){

        var game = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};

        gameService.setCurrentGame([], game, true);
//        $httpBackend.when('GET', 'http://mahjongmayhem.herokuapp.com/games/' + game.id + '/tiles/matches')

        $httpBackend.expectGET('http://mahjongmayhem.herokuapp.com/games/' + game.id + '/tiles/matches')
            .respond(200, {tiles: finishedTiles});

        var response = gameService.history();
        $httpBackend.flush();

        response.$$state.value.data.tiles.should.be.an('array');
        response.$$state.value.status.should.equal(200);

    });

    it('should send a pair of tiles to the backend and respond with status 200', function(){
        var game = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};
        gameService.setCurrentGame([], game, true);

        var tileA = {"xPos":15,"yPos":9,"zPos":1,"tile":{"_id":80,"suit":"Character","name":"3","matchesWholeSuit":false,"__v":0,"id":"80"},"_id":"55ae8950056e5b110042f026"};
        var tileB = {"xPos":17,"yPos":15,"zPos":0,"tile":{"_id":81,"suit":"Character","name":"3","matchesWholeSuit":false,"__v":0,"id":"81"},"_id":"55ae8950056e5b110042f00c"};

        $httpBackend.expectPOST('http://mahjongmayhem.herokuapp.com/games/' + game.id + '/tiles/matches', {tile1Id: tileA._id, tile2Id: tileB._id})
            .respond(200);

        var response = gameService.match(tileA, tileB);
        $httpBackend.flush();

        response.$$state.value.status.should.equal(200);
    });


})

/**
 * Created by ronald on 22-8-15.
 */


describe('gamesService functionality', function(){

    it('should get all games', function(){
        $httpBackend.expectGET('http://mahjongmayhem.herokuapp.com/games')
            .respond(200, []);
        var response = gamesService.all();
        $httpBackend.flush();

        response.$$state.value.data.should.be.an('array');
        response.$$state.value.status.should.equal(200);
    });

    it('should get a specific game', function(){
        var gameToTest = {_id: "12345", template: {}, players: []};
        $httpBackend.expectGET('http://mahjongmayhem.herokuapp.com/games/12345')
            .respond(200, gameToTest);
        var response = gamesService.get(gameToTest);
        $httpBackend.flush();

        response.$$state.value.data.should.be.an('object');
        response.$$state.value.data._id.should.equal(gameToTest._id);
        response.$$state.value.status.should.equal(200);
    });

    it('should add a new game', function(){
        var gameToTest = {_id: "12345", template: {}, players: []};

        $httpBackend.expectPOST('http://mahjongmayhem.herokuapp.com/games')
            .respond(200);

        var response = gamesService.new(gameToTest);

        $httpBackend.flush();

        response.$$state.value.status.should.equal(200);
    });

    it('should get all tiles that arent matched', function(){
        var gameToTest = {_id: "12345", template: {}, players: [], state: "playing"};

        $httpBackend.expectGET('http://mahjongmayhem.herokuapp.com/games/' + gameToTest._id + '/tiles?matched=false')
            .respond(200, [tiles]);

        var response = gamesService.tiles(gameToTest);

        $httpBackend.flush();
        expect(response.$$state.value.data.length).to.be.above(0);
        response.$$state.value.data.should.be.an('array');
        response.$$state.value.status.should.equal(200);
    });

    it('should join a player to specific game', function(){

        var gameToTest = {id: "12345", template: {}, players: []};

        $httpBackend.expectPOST('http://mahjongmayhem.herokuapp.com/games/' + gameToTest.id + '/players')
            .respond(200);

        var response = gamesService.join(gameToTest);

        $httpBackend.flush();
        response.$$state.value.status.should.equal(200);
    });

    it('should start an existing game', function(){
        var gameToTest = {id: "12345", template: {}, players: []};

        console.log(gameToTest);
        $httpBackend.expectPOST('http://mahjongmayhem.herokuapp.com/games/' + gameToTest.id + '/start')
            .respond(200);

        var response = gamesService.start(gameToTest);

        $httpBackend.flush();

        response.$$state.value.status.should.equal(200);
    })



});
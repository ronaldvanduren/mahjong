/**
 * Created by ronald on 22-8-15.
 */
var game = {id: "", template: {}, players: [{_id: "r.vanduren@test.com", name: "Ronald van Duren"}, {_id: "test@test.com", name: "TestPeron"}], minPlayers: 1, maxPlayers: 3};
var game2 = {id: "", template: {}, players: [{_id: "p.vantest@test.com", name: "Peter van Test"}, {_id: "test@test.com", name: "TestPeron"}], minPlayers: 1, maxPlayers: 3};
describe('gamesController functionality', function(){
    it('should return true if the user is already in a game he wants to join', function(){

        gamesController = $controller('GamesCtrl', {$scope: scope, $state: $state, Authentication: authenticationService, Games: gamesService, Game: gameService, Users: userService});

        console.log(authenticationService.currentUser().name);
        expect(gamesController.findUserInGame(game)).to.equal(true);
    });

    it('should return false if the user is not in a game he wants to join', function(){

        gamesController = $controller('GamesCtrl', {$scope: scope, $state: $state, Authentication: authenticationService, Games: gamesService, Game: gameService, Users: userService});

        console.log(authenticationService.currentUser().name);
        expect(gamesController.findUserInGame(game2)).to.equal(false);
    });

    it('should return true if the user joined a specific game', function(){
        gamesController = $controller('GamesCtrl', {$scope: scope, $state: $state, Authentication: authenticationService, Games: gamesService, Game: gameService, Users: userService});
        var game = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};

       gamesController.joinGame(game);

    });

    it('should set the current game and go to the gameboard', function(){
        gamesController = $controller('GamesCtrl', {$scope: scope, $state: $state, Authentication: authenticationService, Games: gamesService, Game: gameService, Users: userService});
        var game = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};

        gamesController.goToGameBoard([], game, true);
        expect(gameService.getCurrentGame().id).to.equal(game.id);
    });

    it('should start a game', function(){
        gamesController = $controller('GamesCtrl', {$scope: scope, $state: $state, Authentication: authenticationService, Games: gamesService, Game: gameService, Users: userService});
        var game = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};

        gamesController.games = [];
        gamesController.games.push(game);
        gamesService.start = function(game){
            return {
                then : function(callback){
                    {
                        callback({status: 200, data:tiles});
                    }
                    return {
                        catch : function(callback){
                            callback({status: 400, data: "error"});
                        }
                    }
                }
            }
        };

        gamesController.startGame(game);

        expect(gameService.getCurrentGame().id).to.equal(game.id);
        expect(gameService.getCurrentGame().isPlayable).to.equal(true);

    });

    it('should set the current game', function(){
        gamesController = $controller('GamesCtrl', {$scope: scope, $state: $state, Authentication: authenticationService, Games: gamesService, Game: gameService, Users: userService});
        var game = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};

        gamesService.tiles = function(game){
            return {
                then : function(callback){
                    {
                        callback({status: 200, data: tiles});
                    }
                }
            }
        };

        gamesController.setCurrentGame(game);

        expect(gameService.getCurrentGame().id).to.equal(game.id);
        expect(gameService.getCurrentGame().tiles).to.be.an('array');
        expect(gameService.getCurrentGame().tiles.length).to.be.above(0);

    });

    it('should set a non-playable game', function(){
        gamesController = $controller('GamesCtrl', {$scope: scope, $state: $state, Authentication: authenticationService, Games: gamesService, Game: gameService, Users: userService});
        var game = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};

        gamesService.tiles = function(game){
            return {
                then : function(callback){
                    {
                        callback({status: 200, data: tiles});
                    }
                }
            }
        };

        gamesController.watchGame(game);

        expect(gameService.getCurrentGame().id).to.equal(game.id);
        expect(gameService.getCurrentGame().isPlayable).to.equal(false);
        expect(gameService.getCurrentGame().tiles).to.be.an('array');
        expect(gameService.getCurrentGame().tiles.length).to.be.above(0);
    });




});
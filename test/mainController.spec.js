/**
 * Created by ronald on 22-8-15.
 */

describe('mainController functionality', function(){
    it('should ad a new game', function(){
        mainController = $controller('MainCtrl', {$rootScope: $rootScope, $state: $state, $scope: scope, Games: gamesService, Games: gamesService, Templates: templateService ,Authentication: authenticationService,});
        var newGame = {id: "55ae8950056e5b110042efb9", tiles: [], template: {}, players:[]};

        gamesService.new = function(game){
            return {
                then : function(callback){
                    {
                        callback({status: 200, data: newGame});
                    }
                    return {
                        catch :function(callback){
                            callback({status: 400, data: "error"});
                        }
                    }
                }
            }
        };

        mainController.addGame(newGame);

        mainController.games.should.be.an('array');
        mainController.games.length.should.equal(1);
        mainController.games[0].id.should.equal(newGame.id);
    });

    it('should redirect to avans auth', function(){
        mainController = $controller('MainCtrl', {$rootScope: $rootScope, $state: $state, $scope: scope, Games: gamesService, Games: gamesService, Templates: templateService ,Authentication: authenticationService,});

        mainController.login();

        console.log(window.location.href);
    })
});
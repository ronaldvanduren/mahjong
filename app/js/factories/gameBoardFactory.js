/**
 * Created by ronald on 14-8-15.
 */

angular.module('factories.Gameboard', [])
    .factory('GameBoard', function(){
        var gameBoard = new Array();
        var columnSize = 80;
        var rowSize = 80;

        var x = new Array();
        var maxX = 0;
        var maxY = 0;

        return {
            init : function(){
                gameBoard = [];
                for(var i = 0; i < rowSize; i++) {
                    var tempArray = [];
                    for(var j = 0; j < columnSize; j++){
                        tempArray.push([]);
                    }
                    gameBoard.push(tempArray);
                }
                return gameBoard;
            }
        }
    });
/**
 * Created by ronald on 13-7-15.
 */

angular.module('services.Users', [])

    .service('Users', function () {
        var currentUser = {
            "_id": "r.vanduren@student.avans.nl", // Avans username
            "name": "Ronald van Duren", // fullname
            "email": "rduren@avans.nl", // avans e-mail
            "nickname": "Default" // maybe filled later?
        };

        return {
            currentUser: function(){
                return currentUser;
            }
        }
    });

/**
 * Created by ronald on 15-7-15.
 */

angular.module('services.Sockets', ['config.Server'])
    .service('Sockets', function($rootScope){

     var socket = null;

        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data, callback) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                })
            },
            connect : function(game){

                console.log('Connecting...' + game.id);
                socket = io('http://mahjongmayhem.herokuapp.com?gameId=' + game.id);
                socket.on('connect', function(){
                    console.log("Socket connection established!");
                    console.log(socket);
                });

                socket.on('reconnect', function(){
                    console.log("Socket connection established!");
                    console.log(socket);
                });
                console.log(socket);

                socket.on('connect_error', function(error){
                    console.log(error);
                    socket = io('http://mahjongmayhem.herokuapp.com?gameId=' + game.id);
                })
            },

            get: function(){
                return socket;
            }
        };
    });

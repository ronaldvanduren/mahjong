/**
 * Created by ronald on 14-8-15.
 */

angular.module('services.Game', ['config.Server'])
    .service('Game',
    function($http, Config) {

        var currentGame = {
            id: "",
            tiles : [],
            players : [],
            isPlayable: true
        };

        return {
            setCurrentGame : function(tiles, game, playable){
                currentGame.tiles = tiles;
                currentGame.players = game.players;
                currentGame.id = game.id;
                currentGame.isPlayable = playable;
            },

            getCurrentGame : function(){
                return currentGame;
            },

            clearCurrentGame: function(){
                this.setCurrentGame([], {players: [], id: ""}, true);
            },

            match : function(tile1, tile2){
                return $http({
                    method: 'POST',
                    url: Config.backend + '/games/' + currentGame.id + '/tiles/matches',
                    data: {tile1Id: tile1._id, tile2Id: tile2._id}
                })
            },

            history : function(){
                return $http({
                    method: 'GET',
                    url: Config.backend + '/games/' + currentGame.id + '/tiles/matches'
                })
            },
        }

    });
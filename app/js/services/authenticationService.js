/**
 * Created by ronald on 15-7-15.
 */

angular.module('services.Authentication', [])
    .service('Authentication', function($rootScope){

        var user = {
            name: window.localStorage.getItem("name"),
            token: window.localStorage.getItem("token")
        };

        return {
            checkLoggedIn : function(){
                if(user.token) {
                    return user.token.length > 0;
                }else{
                    return false;
                }
            },
            logout : function(){
                window.localStorage.removeItem('token');
                window.localStorage.removeItem('username');
                $rootScope.isLoggedIn = false;
                location.reload();
            },
            currentUser : function(){
                return user;
            },
            setCurrentUser: function(id, token){
                //user.name = id;
               // user.token = token;
                window.localStorage.setItem("name", id);
                window.localStorage.setItem("token", token);

                user.name = window.localStorage.getItem("name");
                user.token = window.localStorage.getItem("token");
            }
        }
    });

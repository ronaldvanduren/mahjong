/**
 * Created by ronald on 13-7-15.
 */

angular.module('services.Games', ['config.Server'])
    .service('Games',
    function($http, Config, Authentication) {
        return {
            all: function() {
                return $http({
                    method: 'GET',
                    url: Config.backend + '/games'
                })
            },
            get: function(game){
                return $http({
                    method: 'GET',
                    url: Config.backend + '/games/' + game._id
                })
            },
            new : function(game){
                return $http({
                    method: 'POST',
                    url: Config.backend + '/games',
                    data: JSON.stringify(game)
                })
            },
            tiles : function(game){ //Game param
                return $http({
                    method: 'GET',
                    url: Config.backend +  '/games/' + game._id + '/tiles?matched=false'
                })
            },
            join : function(game){
              return $http({
                  method: 'POST',
                  url:  Config.backend + '/games/' + game.id + '/players'
              })
            },
            start : function(game){
                return $http({
                    method: 'POST',
                    url: Config.backend + '/games/' + game.id + '/start'
                })
            },
            playedGames: function(id){
                return $http({
                    method: "GET",
                    url: Config.backend + '/games?player=' + id + '&state=playing' //+'&state=finished'
                })
            },
            myGames : function(){
                return $http({
                    method: 'GET',
                    url: Config.backend + '/games?createdBy='+Authentication.currentUser().name
                })
            },
            watchableGames : function(){
                return $http({
                    method : 'GET',
                    url: Config.backend + '/games?state=playing'
                })
            }
        }
    });
/**
 * Created by ronald on 13-7-15.
 */

angular.module('config.Server', [])
    .constant('Config', {
        'backend': 'http://mahjongmayhem.herokuapp.com',
        'authentication' : "http://mahjongmayhem.herokuapp.com/auth/avans?callbackUrl=http://localhost:8080/%23/authCallback",
        'version': 0.1
    });

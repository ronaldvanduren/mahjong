/**
 * Created by ronald on 14-7-15.
 */

/**
 * Created by ronald on 13-7-15.
 */

angular.module('services.Templates', ['config.Server'])
    .service('Templates',
    function($http, Config) {
        return {
            all: function() {
                return $http({
                    method: 'GET',
                    url: Config.backend + '/GameTemplates'
                })
            },
            get: function(template){
                return $http({
                    method: 'GET',
                    url: Config.backend + '/GameTemplates/' + template._id
                })
            }
        }
    });
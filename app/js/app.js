/**
 * Created by Ronald on 25-4-2015.
 */

require('angular/angular');
require('angular-ui-router');
require('angular-socket-io-master');
require('./controllers/mainController');
require('./controllers/authenticationController');
require('./controllers/gameController');
require('./controllers/gamesController');
require('./services/config');
require('./services/gamesService');
require('./services/gameService');
require('./services/userService');
require('./services/socketService.js');
require('./services/templateService');
require('./services/authenticationService');
require('./directives/tileDirective');
require('./factories/gameBoardFactory.js');



var app = angular.module('Mahjong', ['controllers.Main', 'controllers.Authentication',
    'controllers.Game', 'controllers.Games', 'services.Games', 'services.Game', 'services.Authentication', 'services.Sockets',
    'services.Users', 'services.Templates', 'config.Server', 'directives.Tile', 'factories.Gameboard', 'btford.socket-io', 'ui.router'])

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        //$urlRouterProvider.otherwise('/home');

        $stateProvider

            //.state('loading', {
            //    url: '/loading',
            //    templateUrl: './loadingPage.html',
            //    controller: 'MainCtrl'
            //})
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home', {
                url: '/home',
                templateUrl: './partials/partial-home.html',
                controller: 'MainCtrl as mainCtrl'
            })

            // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
            .state('games', {
                url: '/games',
                templateUrl: './partials/partial-games.html',
                controller: 'GamesCtrl as gamesCtrl'
            })

            .state('game', {
                url: '/game',
                templateUrl: './partials/partial-game.html',
            })

            .state('login', {
                url: '/login',
                templateUrl: './partials/partial-login.html',
                controller: 'LoginCtrl'
            })

            .state('authCallback', {
                url: '/authCallback',
                templateUrl: './partials/partial-auth.html',
                controller: 'AuthCtrl'
            })

            .state('game.gameboard', {
                url: '/gameboard',
                templateUrl: './partials/game-gameboard.html',
            })

            .state('game.history', {
                url: '/history',
                templateUrl: './partials/game-history.html',
            });


        $httpProvider.interceptors.push(['$q', '$window', 'Authentication', function($q, $window, Authentication) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if (Authentication.currentUser().token != "") {
                        config.headers['x-token'] = Authentication.currentUser().token;
                        config.headers['x-username'] =  Authentication.currentUser().name;

                    }
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        Authentication.logout();
                    }
                    return $q.reject(response);
                }
            };
        }]);

       // $httpProvider.interceptors.push('jwtInterceptor');

    })
    .run(function ($state, $rootScope, $location, Authentication) {

        //// enumerate routes that don't need authentication
        //var routesThatDontRequireAuth = ['/login'];
        //
        //// check if current location matches route
        //var routeClean = function (route) {
        //    return _.find(routesThatDontRequireAuth,
        //        function (noAuthRoute) {
        //            return _.str.startsWith(route, noAuthRoute);
        //        });
        //};
        //
        //
        $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {

            if(!Authentication.checkLoggedIn()){
               // $state.go('home');
            }
            //// if route requires auth and user is not logged in
            ////if (!Authentication.isLoggedIn()) {
            ////    // redirect back to login
            ////    $location.path('/login');
            ////}
            //console.log("ok");
            //
            //if($location.path == '/authCallback'){
            //    alert($location.path());
            //    console.log($location.path());
            //    //TODO get params from url
            //    $location.path = '/home'
            //}else{
            //    alert($location.path());
            //}
        })
    });

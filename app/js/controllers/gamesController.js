/**
 * Created by ronald on 14-8-15.
 */

angular.module('controllers.Games', [])
    .controller('GamesCtrl', function($scope, $state, Authentication, Games, Game, Users){
        var self = this;

        if(!Authentication.checkLoggedIn()){
            $state.go('home');
        }else{
            self.currentUser = Authentication.currentUser();
        }

        Games.all().then(function(resp){
            console.log(resp.games);
            // console.log(3);
            self.games = resp.data;
        });

        Games.watchableGames().then(function(resp){
            if(resp.status === 200){
                self.watchableGames = resp.data;
            }
        });

        Games.playedGames(Authentication.currentUser().name).then(function(resp){
            console.log(resp);
            if(resp.status === 200) {
                self.playedGames = resp.data;
            }
        });

        Games.myGames().then(function(resp){
            if(resp.status === 200){
                self.myGames = resp.data;
            }
        });

        this.watchGame = function(game){
            Games.tiles(game).then(function(resp){
                if(resp.status === 200){
                    Game.setCurrentGame(resp.data, game, false);
                    $state.go('game.gameboard');                }
            });
        };

        this.joinGame = function(game){
            if(Authentication.checkLoggedIn()) {
                if(!self.findUserInGame(game) && game.players.length < game.maxPlayers) {
                    Games.join(game).then(function (data) {
                        if (data.status === 200) {
                            game.players.push(Users.currentUser());
                            $('#messages').empty().append('<div class="message success">You successfully entered the game!</div>');
                        }
                    });
                }else{
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    $('#messages').empty().append('<div class="message error">You are already in this game</div>');
                }
            }
        };

        this.startGame = function(game){
            if(Authentication.checkLoggedIn()){
                //console.log("ok!");
                Games.start(game).then(function(resp){
                   // console.log("ok");
                    if(resp.status === 200){
                        self.games.splice(self.games.indexOf(game), 1);
                        Game.setCurrentGame(resp.data, game, true);
                        $state.go('game.gameboard');
                    }else{
                    //    console.log("ok!");
                    }
                }).catch(function(resp){
                    $('html, body').animate({ scrollTop: 0 }, 'fast');
                    $('#messages').empty().append('<div class="message error">'+ resp.data.message +'</div>');
                });
            }
        };

        this.setCurrentGame = function(game){
            Games.tiles(game).then(function(resp){
                if(resp.status === 200){
                    Game.setCurrentGame(resp.data, game, true);
                    $state.go('game.gameboard');
                }
            });
        };

         this.findUserInGame = function(game){
            if(game.players.length > 0){
                for(j = 0; j < game.players.length; j++){
                    if(game.players[j]._id === Authentication.currentUser().name){
                        console.log("User found");
                        return true;
                    }
                }
                return false;
            }else{
                return false;
            }
        };

        this.goToGameBoard = function(data, game, playable){
            Game.setCurrentGame(data, game, playable);
            $state.go('game.gameboard');
        }

    });
/**
 * Created by ronald on 13-7-15.
 */

angular.module('controllers.Main', [])

    .controller('MainCtrl', function($rootScope, $state, $scope, Games, Templates, Config, Authentication){

        var styles = ['css/wood_theme.css', 'css/bamboo_theme.css'];
        var curStyle = 0;
        this.stylePath = styles[0];
        if(!Authentication.checkLoggedIn()){
            $state.go('home');
        }else{
            $scope.isLoggedIn = true;
            $scope.$apply();
            console.log($scope.isLoggedIn);
            $scope.currentUser = Authentication.currentUser();
        }

        var self = this;
        var isLoaded = false;
        this.games = [];
        var loaded = 0;
        if(isLoaded){
            $state.go('home');
        }
        $scope.newGame = {
            template: "",
            minPlayers: 0,
            maxPlayers: 0
        };

        this.changeStyle = function(){
            console.log("ok");
            if(curStyle == 0){
                self.stylePath = styles[1];
                curStyle = 1;
            }else{
                self.stylePath = styles[0];
                curStyle = 0;
            }

            console.log(self.stylePath);
        };

        this.login = function () {
            window.location.href = Config.authentication;
        };

        this.logout = function(){
            console.log("ok");
            Authentication.logout();
        };

        Templates.all().then(function(data){
           $scope.templates = data.data;
           $scope.newGame.template = data.data[0];
            //$scope.isLoaded = true;
            //$state.go('home');
        });

         Games.all().then(function(data){
            // console.log(3);
             self.games = data.data;
         });

        $scope.minPlayers = 1;
        $scope.maxPlayers = 35;
       // $scope.layouts = Layouts.all();

        this.addGame = function(newGame) {
            var newGame = {"templateName": newGame.template._id, "minPlayers": newGame.minPlayers,"maxPlayers": newGame.maxPlayers};
            $('#messages').empty();
            Games.new(newGame).then(function(resp){
                if(resp.status === 200){
                    console.log(resp);
                    self.games.push(resp.data);  //TODO make sure the new game gets inserted at the top of the list  Refilter?
                    $scope.newGame = {
                        template: "",
                        minPlayers: 0,
                        maxPlayers: 0
                    };
                    $('#messages').append('<div class="message success">' + 'Game succesfully created!' + '</div>');
                }
            }).catch(function(resp){
                console.log(resp);
                $('#messages').append('<div class="message error">' + 'Something went wrong:\n '+ resp.data.message + '</div>');
            });
        };

    });
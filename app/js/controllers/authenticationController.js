/**
 * Created by ronald on 14-7-15.
 */

angular.module('controllers.Authentication', [])

    .controller('AuthCtrl', function AuthCtrl($scope, $location, $state, Authentication){

        $scope.timer = 1;
        $scope.username = $location.search().username;

        Authentication.setCurrentUser($location.search().username, $location.search().token);
       // window.localStorage.setItem('username', $location.search().username);
       // window.localStorage.setItem('token', $location.search().token);

        console.log(Authentication.currentUser().name);
        console.log(Authentication.currentUser().token);

        setInterval(function(){
            $scope.timer += 1;
            if($scope.timer == 2){
               // $window.location.href('/home');
                $state.go('home');
            }
        }, 2000);

    });

/**
 * Created by ronald on 15-7-15.
 */

angular.module('controllers.Game', [])
    .controller('GameCtrl', function($scope, $state, Authentication, Games, Game, GameBoard, Sockets){
//        Authentication.checkLoggedIn();
        if(!Authentication.checkLoggedIn()){
            $state.go('home');
        }
        //TODO don't reconnect when going to history
        var self = this;
        $scope.players = [];
        this.selectedPlayer = null;
        this.history = [];
        this.tiles = [];

        if(Game.getCurrentGame().id === ""){
            $state.go('games');
        }else{
            Sockets.connect(Game.getCurrentGame());

            Sockets.on('match', function(resp){
                console.log("MATCH");
                console.log(resp);
                if(resp[0].match.foundBy != Authentication.currentUser().name) {
                    console.log("removing tiles");
                    removeTileFromScope(resp);
                }

                for(var i = 0; i < $scope.players.length; i++){
                    if( $scope.players[i].id == resp[0].match.foundBy){
                        resp[0].tile = searchForTile(resp[0].tile);
                        console.log(resp[0].tile);
                        $scope.players[i].matchedTiles.push(resp[0]);
                        $scope.players[i].matchedTiles.push(resp[0]);
                        console.log($scope.players[i].matchedTiles);
                        break;
                    }
                }

                self.checkIfMatchesArePossible(self.tiles);
            });

            Sockets.on('end', function(){
                Game.clearCurrentGame();
                $state.go('games');
            });
        }

        this.updateView = function(player){
            self.selectedPlayer = player;

            console.log(this.selectedPlayer);
        };

        this.checkTop = function(tile){

            // console.log(gameBoard[tile.yPos][tile.xPos]);
            if(gameBoard[tile.yPos][tile.xPos][0].zPos > tile.zPos){
                //      console.log("top not free");
                return false;
            }else if(gameBoard[tile.yPos + 1][tile.xPos][0].zPos > tile.zPos){
                //     console.log("top not free");
                return false;
            }else if(gameBoard[tile.yPos][tile.xPos + 1][0].zPos > tile.zPos){
                //       console.log("top not free");
                return false;
            }else if(gameBoard[tile.yPos + 1][tile.xPos + 1][0].zPos > tile.zPos){
                //     console.log("top not free");
                return false;
            }

            //   console.log("top free");
            return true;
        };

        this.checkRightSide = function(tile){

            if(gameBoard[tile.yPos][tile.xPos + 2].length > 0){
                if(gameBoard[tile.yPos][tile.xPos + 2][0].zPos >= tile.zPos){
                    //       console.log("right side not free");
                    return false;
                }
            }

            if(gameBoard[tile.yPos + 1][tile.xPos + 2].length > 0){
                if(gameBoard[tile.yPos + 1][tile.xPos + 2][0].zPos >= tile.zPos){
                    //      console.log("right side not free");
                    return false;
                }
            }

            //console.log("right side free");
            return true;
        };

        this.checkLeftSide = function(tile){

            if(gameBoard[tile.yPos][tile.xPos - 1].length > 0){
                if(gameBoard[tile.yPos][tile.xPos - 1][0].zPos >= tile.zPos){
                    //     console.log("left side not free");
                    return false;
                }
            }

            if(gameBoard[tile.yPos + 1][tile.xPos - 1].length > 0){
                if(gameBoard[tile.yPos + 1][tile.xPos - 1][0].zPos >= tile.zPos){
                    //     console.log("left side not free");
                    return false;
                }
            }
            //  console.log("left side free");
            return true;
        };

        /**
         * Adds a tile on the game board data grid.
         * Adds the tile on a square of four positions to take the overlapping tiles into account
         * @param tiles
         */
        this.addTilesToGameBoard = function(tiles){
            //Clear the current gameboard

            tiles.forEach(function(tile){
                tile.matched = false;
                gameBoard[tile.yPos][tile.xPos].push(tile);
                gameBoard[tile.yPos + 1][tile.xPos].push(tile);
                gameBoard[tile.yPos + 1][tile.xPos + 1].push(tile);
                gameBoard[tile.yPos][tile.xPos + 1].push(tile);
            });

            //  console.log(gameBoard);
            self.tiles = tiles;
        };

        var gameBoard = GameBoard.init();

       /// console.log(Game.getCurrentGame());
        this.addTilesToGameBoard(Game.getCurrentGame().tiles);

        var stroke = "8px solid #f7f7f7";

        var match = new Array();


        Game.history().then(function(resp){
            if(resp.status === 200){
              //  console.log(resp);
                self.history = resp.data;
                initializePlayers();
            }
        });

        $scope.onClick = function(tile){

            if(Game.getCurrentGame().isPlayable) {
                if (self.checkTop(tile)) {
                    if (self.checkLeftSide(tile) || self.checkRightSide(tile)) {
                        self.checkMatch(tile);
                        console.log(tile);
                    }
                }
            }else{
                $('#messages').empty().append('<div class="message error"> You are only spectating, you cannot match any tiles! </div>');
            }
        };

        this.checkMatch = function(tile){
         //   console.log(document.getElementById(tile._id).className);
           // $('#'+tile._id).addClass('selected-tile');
         //   console.log(document.getElementById(tile._id).className);
            match.push(tile);
            if(match.length === 2){
                //TODO check if match returns OK from backend
                if(match[0].tile.matchesWholeSuit && match[1].tile.matchesWholeSuit){
                    if(match[0].tile.suit == match[1].tile.suit && match[0]._id != match[1]._id){
                        console.log("it's a match!");
                        Game.match(match[0], match[1]);
                        removeTileFromScope(match);
                        match.length = 0;
                        return true;
                    }
                }else if(match[0].tile.suit == match[1].tile.suit && match[0].tile.name == match[1].tile.name
                        && match[0]._id != match[1]._id){
                        console.log("it's a match!");
                        Game.match(match[0], match[1]);
                        removeTileFromScope(match);
                    match.length = 0;
                    return true;
                    }
                removeSelectedTileOverlay(match);
                match.length = 0;
                return false;
            }
            //TODO send match to backend, remove tiles from gameboard;
        };

        function removeSelectedTileOverlay(match){
                match.forEach(function(tile){
                    $('#'+tile._id).removeClass('selected-tile');
                });
        }


        function removeTileFromScope(match){
            match.forEach(function(tile){
                self.tiles.forEach(function(tile2){
                    if(tile._id == tile2._id){
                        tile2.matched = true;
                    }
                });
                removeTileFromGrid(tile);
            });

            self.checkIfMatchesArePossible(self.tiles);
        }

        /**
         * Remove tile from the grid
         * @param tile
         */
        function removeTileFromGrid(tile){
            gameBoard[tile.yPos][tile.xPos].splice(gameBoard[tile.yPos][tile.xPos].indexOf(tile), 1);
            gameBoard[tile.yPos + 1][tile.xPos].splice(gameBoard[tile.yPos + 1][tile.xPos].indexOf(tile), 1);
            gameBoard[tile.yPos + 1][tile.xPos + 1].splice(gameBoard[tile.yPos + 1][tile.xPos + 1].indexOf(tile), 1);
            gameBoard[tile.yPos][tile.xPos + 1].splice(gameBoard[tile.yPos][tile.xPos + 1].indexOf(tile), 1);
        }

        /**
         * Checks if a tile is free, if that tile is free it searches
         * for a matching tile and checks if that tile is also free.
         * If both tiles a free a match can be made and true will be returned.
         * @param tileArray
         * @returns {boolean}
         */
        this.checkIfMatchesArePossible = function(tileArray){
            var tiles = [];
            var matches = [];

            //Copy matchable tiles from scope to local array
            //otherwise the scope is altered
            tileArray.forEach(function(tile){
                if(!tile.matched) {
                    tiles.push(tile);
                }
            });

            tiles.forEach(function(tile1){

                if(self.checkTop(tile1)){
                    if(self.checkLeftSide(tile1) || self.checkRightSide(tile1)) {

                        tiles.forEach(function (tile2) {
                            if (tile2.tile.matchesWholeSuit) {
                                if (tile1.tile.suit === tile2.tile.suit && tile1.tile._id !== tile2.tile._id) {
                                    if (self.checkTop(tile2)) {
                                        if (self.checkLeftSide(tile2) || self.checkRightSide(tile2)) {
                                            matches.push({tile1: tile1, tile2: tile2});
                                            tiles.splice(tiles.indexOf(tile1), 1);
                                            tiles.splice(tiles.indexOf(tile2), 1);
                                        }
                                    }
                                }
                            } else if (tile1.tile.suit === tile2.tile.suit && tile1.tile.name === tile2.tile.name
                                && tile1.tile._id !== tile2.tile._id) {
                                if (self.checkTop(tile2)) {
                                    if (self.checkLeftSide(tile2) || self.checkRightSide(tile2)) {
                                        matches.push({tile1: tile1, tile2: tile2});
                                        tiles.splice(tiles.indexOf(tile1), 1);
                                        tiles.splice(tiles.indexOf(tile2), 1);
                                    }
                                }
                            }
                        })
                    }
                }
            });

            if(matches.length > 0){
                console.log("there are still " + matches.length + " matches possible");
                return true;
            }
          //  var winner = self.searchForWinner();
           // $('#messages').empty().append('<div class="message success">No more matches possible, the winner is:' + winner.name + ' with ' + (winner.matchedTiles.length / 2 ) + 'matches! returning to lobby now.</div>');
            $('#messages').empty().append('<div class="message success">There is a winner! Returning to lobby now</div>');

            setTimeout(function(){
                $state.go('games');
            }, 6000);
            return false;
        };

         this.showHint = function() {
            var tiles = [];

            //Copy matchable tiles from scope to local array
            //otherwise the scope is altered
            self.tiles.forEach(function (tile) {
                if (!tile.matched) {
                    tiles.push(tile);
                }
            });

            for(var i = 0; i < tiles.length; i++){
                if (self.checkTop(tiles[i])) {
                    if (self.checkLeftSide(tiles[i]) || self.checkRightSide(tiles[i])) {
                        for(var j = 0; j < tiles.length; j++) {
                            if (tiles[j].tile.matchesWholeSuit) {
                                if (tiles[i].tile.suit == tiles[j].tile.suit && tiles[i]._id != tiles[j]._id) {
                                    if (self.checkTop(tiles[j])) {
                                        if (self.checkRightSide(tiles[j]) || self.checkLeftSide(tiles[j])) {
                                            $('#' + tiles[i]._id).addClass('hint');
                                            $('#' + tiles[j]._id).addClass('hint');
                                            return true;
                                        }
                                    }
                                }
                            }else if (tiles[i].tile.suit == tiles[j].tile.suit && tiles[i].tile.name == tiles[j].tile.name
                                    && tiles[i].tile._id != tiles[j].tile._id) {
                                    if (self.checkTop(tiles[j])) {
                                        if (self.checkRightSide(tiles[j]) || self.checkLeftSide(tiles[j])) {
                                            $('#' + tiles[i]._id).addClass('hint');
                                            $('#' + tiles[j]._id).addClass('hint');
                                            return true;
                                        }
                                    }
                                }
                            }
                    }
                }
            }
            // var winner = self.searchForWinner();
            // $('#messages').empty().append('<div class="message error">No more hints possbile, the winner is:' + winner.name + ' with ' + ( winner.matchedTiles.length / 2 ) + 'matches!</div>');
             $('#messages').empty().append('<div class="message error">No more hints possible!</div>');
             return false;
        };

        function checkIfPlayersIsInGame(game){
            game.players.forEach(function(player){
                if(player.id === Authentication.currentUser().name){

                }
            })
        }

        function initializePlayers(){
            var p = Game.getCurrentGame().players;
            console.log(p);
            for(var i = 0; i < p.length; i++){

                $scope.players.push({id: p[i]._id, name: p[i].name, matchedTiles:[]});
                self.history.forEach(function(matchedTile){
                   // console.log()
                   if(matchedTile.match.foundBy == p[i]._id){
                       $scope.players[i].matchedTiles.push(matchedTile);
                   }
                });
            }

            $scope.$watch('players', function(newData, oldData){
                $scope.players = newData;
            }, true);
            console.log($scope.players);
        }

        function searchForTile(tileId){
            console.log(self.tiles);
            for(var i = 0; i < self.tiles.length; i++){
                if(tileId == self.tiles[i].tile._id){
                    console.log("found matching tile");
                    console.log(self.tiles[i]);
                    return self.tiles[i].tile;
                }
            }
        }

        //this.searchForWinner = function(){
        //    var id;
        //    var highestScore = 0;
        //    for(var i = 0; i < $scope.players.length; i++){
        //        if($scope.players[i].matchedTiles.length > highestScore){
        //            id = i;
        //            highestScore = $scope.players[i].matchedTiles.length;
        //        }
        //    }
        //    return $scope.players[id];
        //}
    });